package clase2;
import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Scanner s = new Scanner (System.in);
		System.out.println("Ingrese el valor ya sea par o impar");
		
		int num=s.nextInt();
		
		if (num % 2 == 0) {
			System.out.println("El n�mero ingresado es par");
		}else { 
			System.out.println("El n�mero ingresado es impar");
		}
	}
}