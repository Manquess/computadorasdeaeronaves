package pac1;

public class Ejercicio3 {
	/**
	 * @param ars
	 */ 
		public static void main (String[]args){
			System.out.println("Tecla de Escape \t \t Significado");
			System.out.println("\\n \t \t \t \t Significa nueva linea ");
			System.out.println("\\t \t \t \t \t Significa una tabulación de espacio ");
			System.out.println("\\\" \t \t \t \t Es para poner “ (comillas dobles) dentro del texto por ejemplo “Belencita\" ");
			System.out.println("\\ \t \t \t \t Se utiliza para poner la \\ dentro del texto, por ejemplo \\algo\\");
			System.out.println("\\\' \t \t \t \t Es para poner las ' (comilla simple) para escribir por ejemplo 'Princesita\' ");
		}
	}
